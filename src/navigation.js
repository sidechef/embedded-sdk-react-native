import * as React from 'react';

/// Handle back button to navigate back the webview.
export function useReactNavigationBack(webView, navigation, options = {
  gestureEnabled: false,
  headerBackTitle: 'Back',
}) {
  const [canGoBack, setCanGoBack] = React.useState(false);

  // Hook on the beforeRemove event to intercept the back action.
  React.useEffect(() => navigation.addListener('beforeRemove', event => {
    if (canGoBack) {
      // Navigate back in the webView.
      event.preventDefault();
      webView.current.goBack();
    }
  }), [navigation, webView.current, canGoBack]);

  // Set navigation options.
  React.useLayoutEffect(() => {
    navigation.setOptions(options);
  }, [navigation]);

  // Returns a callback to update the back state.
  return React.useCallback(state => {
    setCanGoBack(state.canGoBack)
  }, [canGoBack])
}
