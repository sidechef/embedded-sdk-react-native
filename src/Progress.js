import * as React from 'react';
import { Animated, Platform, PlatformColor, StyleSheet, View, useWindowDimensions } from 'react-native';

const defaultColor = Platform.select({
  android: PlatformColor('@android:color/holo_green_light'),
  ios: PlatformColor('systemBlue')
})

const Progress = ({ color = defaultColor, value }) => {
  const { width } = useWindowDimensions();

  const animatedValue = React.useRef(new Animated.Value(0)).current;

  const offset = animatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [-width, 0],
  });

  const opacity = React.useRef(new Animated.Value(1)).current;

  React.useEffect(() => {
    // progress
    Animated.timing(animatedValue, {
      toValue: value,
      duration: 400,
      useNativeDriver: true,
    }).start(() => {
      if (value === 1) {
        // fade out
        Animated.timing(opacity, {
          toValue: 0,
          duration: 200,
          useNativeDriver: true,
        }).start(() => {
          // reset values
          animatedValue.setValue(0)
          opacity.setValue(1)
        });
      }
    });
  }, [value]);

  if (value === 1 && opacity.__getValue() === 0) return null;

  return (
    <View style={styles.progress} pointerEvents="box-none">
      <Animated.View
        style={[
          styles.progressBar,
          { backgroundColor: color },
          { transform: [{ translateX: offset }] },
          { opacity }
        ]}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  progress: {
    position: 'absolute',
    top: 0,
    width: '100%',
    height: 5,
  },
  progressBar: {
    height: 5,
  }
});

export default Progress;
