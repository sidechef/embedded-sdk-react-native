import * as React from 'react';
import { ActivityIndicator, Linking, StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';

import Progress from './Progress';
import * as auth from './authentication';
import { applyBefore, unpack } from './utils'

export const Loading = () => (
  <View style={styles.centered}>
    <ActivityIndicator size="large" />
  </View>
);

const Error = message => (
  <View style={styles.centered}>
    <Text>{message}</Text>
  </View>
)

const openUrl = url => Linking.openURL(url)

const SideChefWebView = React.forwardRef(({
  apiKey,
  userInfo,
  appliances,
  nextPath,
  hasProgress = true,
  progressColor,
  onExternalLink = openUrl,
  renderLoading = Loading,
  renderError = Error,
  ...props
}, ref) => {
  const webView = ref || React.createRef(null)
  const [progressValue, setProgressValue] = React.useState(0)
  const [state, setState] = React.useState(null)
  const { partnerKey, clientKey } = unpack(apiKey)

  React.useEffect(() => {
    async function signin() {
      const result = await auth.authenticate({
        partnerKey,
        clientKey,
        userInfo,
        appliances,
        nextPath
      });
      setState(result)
    }

    signin();
  }, []);

  if (!state) {
    return renderLoading()
  }

  if (state?.error) {
    return renderError(state.error)
  }

  return (
    <View style={styles.webviewContainer}>
      <WebView
        ref={webView}
        source={{
          uri: state.url,
          headers: {
            Cookie: state.cookie,
          },
        }}
        startInLoadingState
        sharedCookiesEnabled
        renderLoading={renderLoading}
        renderError={renderError}
        {...props}
        onShouldStartLoadWithRequest={request => {
          if (props.onShouldStartLoadWithRequest) {
            const shouldStart = props.onShouldStartLoadWithRequest(request)
            if (shouldStart === false) {
              return false
            }
          }
          if (!request.url.startsWith(auth.BASE_URL)) {
            onExternalLink(request.url)
            return false
          }
          return true
        }}
        onLoadStart={applyBefore(props.onLoadStart, () => {
          if (hasProgress) setProgressValue(0)
        })}
        onLoadProgress={applyBefore(props.onLoadProgresss, event => {
          if (hasProgress) setProgressValue(event.nativeEvent.progress)
        })}
      />
      {hasProgress && <Progress value={progressValue} color={progressColor} />}
    </View>
  )
});

const styles = StyleSheet.create({
  centered: {
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  webviewContainer: {
    height: '100%',
    width: '100%',
  },
});

SideChefWebView.Loading = Loading

export default SideChefWebView;
