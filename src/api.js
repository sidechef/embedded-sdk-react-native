import { BASE_URL } from './authentication'
import { getCookies, clearCookies, stringifyCookies } from './cookies';

const fakedata = [
  {
    id: 56408,
    name: "Stir-Fried Tomato with Scrambled Eggs",
    slug_name: "stir_fried_tomato_with_scrambled_eggs",
    cover_pic_url_small: "https://www.sidechef.com/recipe/small/9103268d-f5cc-4415-bf98-37e16513045f.jpg?d=1408x1120",
  },
  {
    id: 123427,
    name: "6-Ingredient BBQ Roasted Cauliflower Bowl",
    slug_name: "6_ingredient_bbq_roasted_cauliflower_bowl",
    cover_pic_url_small: "https://www.sidechef.com/recipe/small/35c08bb0-1741-4364-9d4d-33c76dc4b300.jpg?d=1408x1120",
  }
]

export async function getPopular() {
  try {
    const cookies = await getCookies()
    console.log('using previously stored cookie?', cookies !== null)

    const config = cookies && { headers: { Cookie: stringifyCookies(cookies) }}
    const response = await fetch(`${BASE_URL}/lg/home/popular/?page=1&page_size=12`, config)
    if (!response.ok) {
      console.warn('Response not OK', response)
      return null
    }
    const data = await response.json()
    return data.results.concat(fakedata)
  } catch (error) {
    console.warn('Error', error)
    return null
  }
}

export async function logout() {
  // TODO: should do more here
  await clearCookies()
}
