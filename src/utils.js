import base64 from 'react-native-base64'

export const applyBefore = (propFn, fn) => (...args) => {
  fn(...args)
  if (propFn) {
    propFn(...args)
  }
}

export const unpack = apiKey => {
  const [partnerKey, clientKey] = base64.decode(apiKey).split(':')
  return { partnerKey, clientKey }
}
