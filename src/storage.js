// This module should implement a Façade to different storage implementations.
// FIXME: Just for reference we are using a plain async storage, but we should use a secure storage instead
import AsyncStorage from '@react-native-async-storage/async-storage'

export async function saveItem(key, value) {
  try {
    await AsyncStorage.setItem(key, JSON.stringify(value))
    return true
  } catch (e) {
    console.warn(`error saving ${key}`, e)
    return false
  }
}

export async function getItem(key) {
  try {
    const value = await AsyncStorage.getItem(key)
    return value !== null ? JSON.parse(value) : null
  } catch (e) {
    console.warn(`error getting ${key}`, e)
    return null
  }
}

export async function removeItem(key) {
  try {
    await AsyncStorage.removeItem(key)
    return true
  } catch (e) {
    console.warn(`error removing ${key}`, e)
    return false
  }
}
