import { JSHmac, CONSTANTS } from 'react-native-hash';
import setCookie from 'set-cookie-parser';

import { parseCookies, stringifyCookies, saveCookies, getCookies } from './cookies'

export const BASE_URL = 'https://stageapi.sidechef.com';

async function getSignature(key, timestamp, ...fields) {
  const message = [...fields, timestamp].join('')
  return await JSHmac(message, key, CONSTANTS.HmacAlgorithms.HmacSHA256);
}

export function getAuthUrl(partnerKey, redirect = true) {
  let url = `${BASE_URL}/${partnerKey}/account/signup/`
  if (redirect) {
    url += '?redirect=1'
  }
  return url
}

export async function getAuthPayload({ clientKey, userInfo, appliances }) {
  const timestamp = new Date().toISOString();
  const signature = await getSignature(clientKey, timestamp, userInfo.id, userInfo.photo);
  return JSON.stringify({
    username: `lg$${userInfo.id}`,
    appliances,
    timestamp,
    signature,
  })
}

export async function authenticate({
  partnerKey,
  clientKey,
  userInfo,
  appliances,
  nextPath,
}) {
  const cookies = await getCookies()
  if (cookies !== null) {
    console.log('using previously stored cookie')

    // TODO: update appliances

    const cookie = stringifyCookies(cookies)
    // FIXME: is it correct to assume redirect to partner home?
    const url = nextPath ? `${BASE_URL}/${partnerKey}/${nextPath}` : `${BASE_URL}/${partnerKey}/`
    return { url, cookie }
  }
  console.log('authenticating ...')
  try {
    const authUrl = getAuthUrl(partnerKey, false)
    const authPayload = await getAuthPayload({ clientKey, userInfo, appliances })
    const response = await fetch(authUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: authPayload,
    })

    console.log('got response')
    if (!response.ok) {
      console.log('response is bad', response)
      let detail;
      try {
        // Try to get error details from the response.
        detail = (await response.json()).error;
      } catch (error) {
        // Fallback to the response body.
        detail = await response.text();
      }

      return {
        error: `Request failed with status code ${response.status}`,
        detail: String(detail),
      };
    }

    console.log('response is good')
    const json = await response.json()
    const cookies = parseCookies(response)

    console.log('saving cookies for later usage', cookies)
    saveCookies(cookies)

    const cookie = stringifyCookies(cookies)
    const url = nextPath ? `${BASE_URL}/${nextPath}` : json.redirect
    console.log('authentication done, next', url)

    return { url, cookie };
  } catch (error) {
    console.log('error', error)
    return { error: error.message };
  }
}
