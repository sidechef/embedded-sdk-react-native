import setCookie from 'set-cookie-parser'

import { saveItem, getItem, removeItem } from './storage'

export function parseCookies(response) {
  return setCookie.parse(
    setCookie.splitCookiesString(
      response.headers.get('set-cookie')
    )
  )
}

export function stringifyCookies(cookies) {
  return cookies.map(cookie => `${cookie.name}=${cookie.value}`).join('; ')
}

const KEY = '@cookie'

export async function saveCookies(cookies) {
  return saveItem(KEY, cookies)
}

export async function getCookies() {
  const cookies = await getItem(KEY)
  if (cookies == null) {
    return null
  }

  const now = new Date()
  const expired = cookie => new Date(cookie.expires) < now
  if (cookies.every(expired)) {
    console.log('cookies are expired!')
    await clearCookies()
    return null
  }

  return cookies
}

export async function clearCookies() {
  console.log('clearing cookies')
  await removeItem(KEY)
}
