import * as React from 'react'
import SideChefWebView from './src/SideChefWebView'
import { useReactNavigationBack } from './src/navigation'
import { applyBefore } from './src/utils'
import * as api from './src/api'

export default SideChefWebView
export { useReactNavigationBack }
export { api }

SideChefWebView.WithNavigation = ({ navigation, ...props }) => {
  const webView = React.useRef(null);
  const registerBackState = useReactNavigationBack(webView, navigation)
  return (
    <SideChefWebView
      ref={webView}
      {...props}
      onNavigationStateChange={applyBefore(props.onNavigationStateChange, registerBackState)}
    />
  );
}
