# SideChef SDK for React Native

This project provides a React Native library to embed SideChef in your app.

In short, you can easily embed SideChef writing code like this:

```jsx
import SideChefWebView from '@sidechef/embedded-sdk-react-native';

// provided by SideChef
const API_KEY = 'bGc6WHVlNHE1c11YQmhXcmU5S3k2bnVhVVN0TlMzQ05V'

// provided by your app
const userInfo = { id: 'paolo' }
const appliances = []

export default function SideChefRecipes() {
  return (
    <SideChefWebView
      apiKey={API_KEY}
      userInfo={userInfo}
      appliances={appliances}
    />
  );
}
```

## Installation

Install the module with `npm` or `yarn`:

```
$ npm install --save @sidechef/embedded-sdk-react-native
```

```
$ yarn add @sidechef/embedded-sdk-react-native
```

## Getting Started

You need to receive you `API_KEY` from [SideChef][business-contact-us].
Then you can use the `SideChefView` component in your app.

We also provide integration with [React Navigation][react-navigation] so that
you can navigate back from your screens:

```jsx
export default function SideChefScreen({ navigation }) {
  return (
    <SideChefView.WithNavigation
      navigation={navigation}
      apiKey={API_KEY}
      userInfo={userInfo}
      appliances={appliances}
    />
  )
}
```

That is equivalent to manually setting a ref and registering a navigation handler like this:

```jsx
import SideChefView, { useReactNavigationBack } from '@sidechef/embedded-sdk-react-native'

export default function SideChefScreen({ navigation }) {
  const ref = React.useRef()
  const registerBackState = useReactNavigationBack(ref, navigation) 
  return (
    <SideChefView
      ref={ref}
      onNavigationStateChange={registerBackState}
      apiKey={API_KEY}
      userInfo={userInfo}
      appliances={appliances}
    />
  )
}
```

## API

### `SideChefView` props

| Property | Description | Type | Required | Default |
| :--- | :--- | :---: | :---: | :---: |
| `apiKey` | The API key provided by SideChef | string | yes |
| `userInfo` | The user object provided by the host app | object | yes |
| `appliances` | The list of appliances to be used by SideChef appliance integration | array | no |
| `hasProgress` | Display a progress indicator bar on the top of the view | boolean | no | `true` |
| `progressColor` | The color of the progress indicator bar | [color][color] | no | platform default |
| `onExternalLink` | A function that is called before following a link, see [react-native-webview Reference][onshouldstartloadwithrequest] | function | no |
| `renderLoading` | A function that renders the loading indicator displayed during the initialization | function | no | platform indicator |
| `renderError` | TODO |

All the properties supported by the `WebView` component from `react-native-webview` are also supported.

[business-contact-us]: https://www.sidechef.com/business/contact-us
[react-navigation]: https://reactnavigation.org/
[color]: https://reactnative.dev/docs/colors
[onshouldstartloadwithrequest]: https://github.com/react-native-webview/react-native-webview/blob/master/docs/Reference.md#onshouldstartloadwithrequest
